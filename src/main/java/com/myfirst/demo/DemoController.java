package com.myfirst.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.BufferedReader;
import java.io.FileReader;


@RestController
public class DemoController {
	
	
	
	@GetMapping("/hello")
	public String sayHello() {
		System.out.println("Hello");
		return "Hello";
	}

	@GetMapping("/hello1")
	public String sayHello1() {
		
		return "Hello1";
	}

	@GetMapping("/hello2")
	public String sayHello2() {
		
		return "Hello2";
	}

	@GetMapping("/hello3")
	public String sayHello3() {
		
		return "Hello3";
	}

	


}
